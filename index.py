# Some python code needs to be added


class User:
    def __init__(self, first_name, age):
        self.first_name = first_name
        self.age = age

    def __str__(self):
        return self.first_name


if __name__ == "__main__":
    user1 = User("John", 20)
    print(user1.age)
